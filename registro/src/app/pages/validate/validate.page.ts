import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.page.html',
  styleUrls: ['./validate.page.scss'],
})
export class ValidatePage implements OnInit {

  form: FormGroup;

  constructor(
    private builder: FormBuilder,
    private db: AngularFirestore,
    private fire: AngularFireAuth

  ) { }

  ngOnInit() { 
    this.validaForm();
    
  }
  validaForm(){
    this.form = this.builder.group({
        nome: ['', [
              Validators.required,
              Validators.minLength(5),
              Validators.maxLength(60)
          ] 
        ],
        email: ['', [
              Validators.email,
              Validators.required
          ]
        ],
        senha: ['',[
            Validators.required,
            Validators.minLength
          ]
        ],
        fone:['',[
            Validators.required,
            Validators.minLength(10)
            ]
        ],
        nasc:['',[
            Validators.required,
            Validators.minLength(10)
            ]
        ]
      })
  }

  nome() {
    return this.form.get('nome');
  }

  criaUsuario(){
    const data = this.form.value;
    this.fire.auth.createUserWithEmailAndPassword(data.email,data.senha).
    then(credential=> {
      credential.user.updateProfile({
        displayName: this.form.value.nome
      });
      const usuario = this.db.collection('usuario');
      usuario.add({
          nome: data.nome,
          fone: data.fone,
          nasc: data.nasc,
          email: data.email,
          id: credential.user.uid
      })
    })
  }

    facebookLogin(){
        const provider = new auth.FacebookAuthProvider();
        this.fire.auth.signInWithPopup(provider);
    } 

}
