import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
	nome;
	email;
	userList = [];
	authState: Observable<firebase.User>;

  constructor(
	private fire: AngularFireAuth,
	private db: AngularFirestore,
	private nav: NavController
  ) {
	  this.authState = fire.authState;
  }

  ngOnInit() {
	  this.authState.subscribe(user=>{
		  if(!user){
			  this.nav.navigateForward('entrar');
		  }
		  this.email = user.email;
		  this.nome = user.displayName;
	  });

	  const usuario = this.db.collection('usuario');
	  usuario.get().subscribe(lista=>{
			lista.forEach(user=>{
				const x = {
					id: user.data().id,
					nome: user.data().nome,
					fone: user.data().fone,
					nasc: user.data().nasc,
					email: user.data().email
				};
				this.userList.push(x);
			});
	  });
  }

}
