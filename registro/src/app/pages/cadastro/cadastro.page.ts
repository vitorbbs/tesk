import { Component, OnInit } from '@angular/core';
import { AlertController, IonItemSliding } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  /*lista = [
    {nome: 'Maria Das Graça', email: 'mdd@gmail.com', idade: 21},
    {nome: 'Maria Das Graça', email: 'mdd@gmail.com', idade: 22},
    {nome: 'Maria Das Graça', email: 'mdd@gmail.com', idade: 23},
  ];
*/

lista: Observable<any[]>;

constructor(
    private db: AngularFirestore,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.lista = this.db.collection('pessoa').valueChanges({idField: 'id'});
  }
  
  async createUser(pessoa?:any){
    const alert = await this.alert.create({
      subHeader: 'Digite os dados do Usuário',
      inputs: [
        {
          type: 'text',
          name: 'nome',
          value: pessoa ? pessoa.nome: '',
          placeholder: 'Nome'
        },
        {
          type: 'email',
          name: 'email',
          value: pessoa ? pessoa.email: '',
          placeholder: 'E-mail'
        },
        {
          type: 'number',
          name: 'idade',
          value: pessoa ? pessoa.idade: '',
          placeholder: 'Idade'
        }
      ],
      buttons: [
        {
          text: 'Cancelar', 
          role: 'cancel'
        },
        {
          text: 'Enviar',
          handler: data => this.send(data, pessoa)
        }
      ]
    });
    alert.present();  
  }
  private send(data, pessoa){
    pessoa ?
    this.db.doc('pessoa/' + pessoa.id).update(data) :
    this.db.collection('pessoa').add(data);
  }

  delete(pessoa, item: IonItemSliding) {
    this.db.doc('pessoa/' + pessoa.id).delete();
    item.close();
  }
  edit(pessoa, item: IonItemSliding) {
    this.createUser(pessoa)
    item.close();
  }
}
