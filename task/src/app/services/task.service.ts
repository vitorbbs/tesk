import { Injectable } from '@angular/core';
import { FakeDataService } from './fake-data.service';
import { Task } from '../interfaces/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  public taskList: Task[];

  constructor(
    //Acesso ao banco de dados
    private data: FakeDataService
  ) { 
    this.taskList = data.get();
  }

  save(task: Task[]) {
    this.data.set(task);
  }
}
